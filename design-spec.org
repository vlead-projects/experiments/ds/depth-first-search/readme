#+TITLE: DESIGN SPEC FOR DEPTH FIRST SEARCH
#+AUTHOR: VLEAD
#+DATE: [2019-08-12 Mon]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: \n:t

This is the desgin document for Depth First Search. This includes :
+ Mapping of experiment level learning objectives to Learning Units(LU).
+ Mapping of LU level learning objectives to tasks.
+ List of artefacts under each task.
+ Design specifications for each artefact.
+ Acceptance testing for artefacts.

* LEARNING OBJECTIVES AT EXPERIMENT LEVEL :
| LEARNING OBJECTIVE | LEARNING UNIT | BLOOM'S TAXONOMY LEVEL |
|-------------------------------------------------------------|
|To recall basics of Graphs | PRE-TEST | Understand|
|To describe graph techniques | PRE-TEST | Remember and understand |
|To describe Depth First Search (DFS) Algorithm | DFS |Remember and Understand |
|To illustrate time and space complexities | ANALYSIS | Analyze |
|To apply the concepts learnt and test understanding of user by attempting the post-test quiz | POST-TEST | Evaluate |

* LEARNING UNITS AND LIST OF TASKS UNDER EACH LEARNING UNIT

** LU 1	: PRE-TEST
|TASK | LU LEARNING OBJECTIVE | 
|-----+-----------------------|
|Recap1 (Graphs) | To review the concepts of Graphs |
|Pre-Test | To test user's understanding of pre requisites |

** LU 2	: DFS
|TASK | LU LEARNING OBJECTIVE |
|-----+-----------------------|
|Intro : Intuition | To show an intuition behind DFS |
|Demo : Graphs | To show the working of algorithm on graphs |
|Exercise | To practice the algorithm and to get complete understanding on how the algorithm works and proceeds |
|Algorithm | To understand the algorithm of DFS |
|Quiz | To test the understanding of user on the DFS algorithm |

** LU 3 : ANALYSIS
|TASK | LU LEARNING OBJECTIVE |
|-----+-----------------------|
| Space and Time Complexity | To explain the space and time complexity of DFS |

| Comparision | To compare BFS with DFS |
|Quiz | To test the understanding of user |

** LU 4 : POST-TEST 
|TASK | LU LEARNING OBJECTIVE |
|-----+-----------------------|
|Post-Test | To test user's understand on DFS Algorithm |

* LIST OF ARTEFACTS IN EACH TASK 

** LU 1 : PRE-TEST

| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Recap1 (Graphs) | Video | Video starts with the definition of Graph and explains about representation of Graphs based on the terms Vertices and Edges . Then it explains about different types of graphs with examples and the terms Indegree , Outdegree ,Connectivity and Reachability with examples .|
| Recap1 (Graphs) | Text and Images | The text should explain the basic Terminilogy used in Graphs , different types of graphs and images acts as examples. |
| Pre-test quiz | MCQ | Each question tests the understanding of graphs . |
 
** LU 2 : DFS

| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Intro | Text and Images | Should explain the intuition of DFS and a detailed introduction of DFS . |
| Demo (Trees) | Interactive element | IE will provide a tree for user and asks the user to click on the node where the user wants to start DFS from. Then the IE produces the output with nodes and edges changing their colour based on their type . And finally all the explored nodes and edges are turned black showing that these are the edges and nodes visited on performing DFS on chosen node and unvisted edges and nodes remains white . |
| Demo (Graphs) |  Interactive element | IE will provide a graph for user and asks the user to click on the node where the user wants to start DFS from. Then the IE produces the output with nodes and edges changing their colour based on their type . And finally all the explored nodes and edges are turned black showing that these are the edges and nodes visited on performing DFS on chosen node and unvisted edges and nodes remains white . |
| Exercise | Interactive Element | IE provides a graph and asks user to perform DFS on that graph by selecting the node from where user wants to start dfs and after completing the dfs user should click the SUBMIT button which is provided at the bottom of the artefact and then user can know whether the dfs performed is valid or not .|
| Algorithm | Video | The video should explain the algorithm of DFS with examples . |
| Algorithm Demo | Video | This video should give a experiment demo of dfs with example .|
| Quiz | MCQ | Each question tests the understanding of dfs algorithm .|

** LU 3 : ANALYSIS
| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Concept | Text | Text should discuss about Space and Time complexity of DFS . |
| Comparision | Video | Video should explain the differences between BFS and DFS with example .|
| Quiz | MCQ | Each question tests the understanding of complexities of dfs and difference between bfs and dfs .|

** LU 4 : POST-TEST 
| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Quiz | MCQ | Each question tests the understanding of DFS algorithm . |

* Acceptance Testing

** Acceptance Testing for Video Artefacts

+ Given video, when I load page, video loads
+ Given video, when I press play button, video plays
+ Given video, when I press pause button, video stops

** Acceptance testing for Interactive Elements

*** Demo artefacts

+ Given artefact, when I click on Generate Graph, a graph is generated.
+ Given artefact, when I click on pause, demonstration pauses
+ Given artefact, the speed can be controlled using speed adjuster
+ Given artefact, when I click on play , dmonstration resumes

*** Excercise artefacts

+ Given artefact , when I click SUBMIT the result is displayed with suitable explanation .

